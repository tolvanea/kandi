"""
This script is used in generating the tikz-figure of random path
"""

import numpy as np

def gencrd(x:int = 0, offset=(0,0), factor=1.0):
    np.random.seed(x)
    circ = np.linspace(0,2*np.pi,16)
    x = np.cos(circ)
    y = np.sin(circ)
    
    for i in range(len(circ)):
    	x[i]          += (np.random.normal())*0.1
    	y[i]          += (np.random.normal())*0.1
    	x[i:(i+2)%16] += (np.random.normal())*0.2
    	y[i:(i+2)%16] += (np.random.normal())*0.2
    	x[i:(i+4)%16] += (np.random.normal())*0.3
    	y[i:(i+4)%16] += (np.random.normal())*0.3
    
    x = x * factor + offset[0]
    y = y * factor + offset[1]

    pairs = ["" for i in range(len(circ))]
    
    for i in range(len(circ)):
        pairs[i] = "{:.3f}/{:.3f}".format(x[i], y[i]);
    
    thestring = ", ".join(pairs) + ", " + pairs[0]
    
    return thestring



left1 = r"""	\draw [color="""
left2 = r"""] (0,0) node[circle,inner sep=2pt,draw, fill=red] {\tiny POISTA}
	\foreach \XX/\YY in {"""
right = r"""}{
		-- (\XX, \YY) node[circle,inner sep=1pt,draw, fill=white] {}
	};"""

print("\n\n")

print(left1 + "black" + left2 + gencrd(0, (-6,-6)) + right + "\n")

print(left1 + "black" + left2 + gencrd(1, (6,6)) + right + "\n")

print(left1 + "black" + left2 + gencrd(0, (-25,0), 3) + right + "\n")


"""Finding a good seed """
"""
print(left1 + "red"   + left2 + gencrd(0) + right + "\n")
print(left1 + "green" + left2 + gencrd(1) + right + "\n")
print(left1 + "blue"  + left2 + gencrd(2) + right + "\n")
print(left1 + "gray"  + left2 + gencrd(3) + right + "\n")
"""

"""
print(left1 + "red"   + left2 + gencrd(4) + right + "\n")
print(left1 + "green" + left2 + gencrd(5) + right + "\n")
print(left1 + "blue"  + left2 + gencrd(6) + right + "\n")
print(left1 + "gray"  + left2 + gencrd(7) + right + "\n")
"""

def zoom_lens_coordinate():
    # calculates coordinates for zoom lens in path-figure
    # Tikz is mostly pain to plot any trigonometry

    AA = (-6.0,-6.0)
    BB = (-25.0, 0.0)

    R_AA = 3.0
    R_BB = 9.0

    x_shift = abs(AA[0]-BB[0])
    y_shift = abs(AA[1]-BB[1])

    d_AA_BB = np.sqrt( x_shift**2 + y_shift**2 )
    rho = np.arccos(R_BB/d_AA_BB)

    print("Debug1", y_shift, x_shift, y_shift/x_shift)

    rho_offset = np.arctan(y_shift/x_shift)

    final = (rho + rho_offset) * 360 / (2*np.pi)
    print("Debug2", d_AA_BB, rho_offset* 360 / (2*np.pi), rho* 360 / (2*np.pi))
    print("angle = {}".format(final))


