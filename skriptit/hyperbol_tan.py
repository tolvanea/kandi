import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
#import natural_constants_SI as nc

fig1, ax1 = plt.subplots(1, 1, num='asdf', figsize=(3,2.4))

x_ax = np.linspace(-2.5,2.5,64);

y = np.tanh(x_ax)

plt.plot(x_ax, y)

#plt.title("$\\color{red} feikki-korrelaatiokäyrä$")
plt.xlabel("$x$")
plt.ylabel("tanh($x$)", rotation=0) # tikz_save does not work properly with rotation, 
                            # add in axis parameters a line: 
                            #    y label style={rotate=-90},
                            # add after \begin{tikzpicture}: "[scale=0.8]"
plt.axis([-2.5,2.5,-1,1])

#fig1.tight_layout()

tikz_save('hyp_tangentti.tikz')
plt.show()
