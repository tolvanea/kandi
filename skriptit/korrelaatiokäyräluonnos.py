import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
import natural_constants_SI as nc

x_ax = np.linspace(-1,1,41);

def C(x:np.ndarray) -> np.ndarray :
    k0 = 10
    x0 = 0
    K = 2
    c = K / (1 + np.exp(k0 * (x0-x))) -1
    
    return c

y = C(x_ax)

plt.plot(x_ax, y)
plt.plot([0.2, 0.2, -1], [-1, y[24], y[24]], color="gray", linestyle="--")

plt.plot([-1, 1], [-1,1], color="gray", linestyle="-",
         linewidth=0.5, zorder=-10) # linestyle=(0, (5, 10))

#plt.title("$\\color{red} feikki-korrelaatiokäyrä$")
plt.xlabel("$P_0$")
plt.ylabel("$\\langle P_1 \\rangle$", rotation=0) # tikz_save does not work properly with rotation, 
                            # add in axis parameters a line: "y label style={rotate=-90},"
                            # add after \begin{tikzpicture}: "[scale=0.8]"
plt.axis([-1,1,-1,1])

tikz_save('feikki_korrelaatiokayra.tikz')
plt.show()
