import numpy as np
import matplotlib.pyplot as plt
import typing as tp
import natural_constants_SI as nc

#from matplotlib2tikz import save as tikz_save



def draw_lines(x_vec:np.ndarray, y_vec:np.ndarray, x_crd:float, 
               ll:float=-1.0, lf=1.0, offset:float=0.0) \
    -> tp.Tuple[np.ndarray, np.ndarray]:
    """
    This function helps to plot a little vertical line to data
    
    Arguments: 
        x_vec:np.ndarray,  x-axis of plot
        y_vec:np.ndarray,  x-axis of plot
        x_crd:float,       where in x-axis does that place lie
        ll:float=-1.0      length of line to be drawn, default use mean
        lf:float=1.0       if 'll' not defined, this is the scaling factor to be used
    
    Usage:
        plt.plot(x_ax, y)
        plt.plot( *draw_lines(x_ax, y,  1e-9), color="black")
        plt.plot( *draw_lines(x_ax, y, -1e-9), color="black")
    
    #TODO increase correct place by linear interpolation of x-y-data
    """
        
    if ll<0:
        ll = np.median(np.abs(y_vec))/5 * lf
        
    separate = lambda place: np.ones(2) * place
    
    get_idx = lambda arr, val: (np.abs(arr-val)).argmin()
    
    draw_it = lambda arrx, arry, val, l: \
        np.array( [arry[get_idx(arrx,val)] -l/2 ,   \
                   arry[get_idx(arrx,val)] +l/2   ] \
                )
    
    return separate(x_crd), draw_it(x_vec, y_vec, x_crd, ll) + offset
    

def V_yks(r:float, R:float) -> float :
    if (r >= R):
        V = nc.k_q * (-nc.e) / r
    else:
        V = nc.k_q * (-nc.e_q) / (2*R) * (3 - r**2 /(R**2))
    return V


x_ax = np.linspace(-8e-9, 8e-9, 100);
R=3e-9

V = np.vectorize(lambda x: V_yks(x, R))
y = V(np.abs(x_ax))

fig, ax1 = plt.subplots(1, 1, num='Kvanttipisteen potentiaalin muoto', figsize=(4.0,2.6))

plt.plot(x_ax, y)
plt.plot( *draw_lines(x_ax, y,  R, lf=1), color="black")
plt.plot( *draw_lines(x_ax, y, -R, lf=1), color="black")


frame1 = plt.gca()
frame1.axes.get_xaxis().set_ticks([0])
frame1.axes.get_yaxis().set_ticks([])
#frame1.axes.get_xaxis().set_visible(False)
#frame1.axes.get_yaxis().set_visible(False)

m = np.min(y) - abs(np.min(y))*0.1
p = draw_lines(x_ax, y,  R, lf=0)[1][1]
plt.plot( [-R, R], [p, p], color="black")

plt.text(R*0.14, p*0.97, "$2 \\, R_{QD}$", {'color': 'k', 
                         'fontsize': 12,
                         'verticalalignment': 'bottom', 
                         'horizontalalignment': 'center'})

plt.text(0,     p*1.3, "$V \\propto r^2$", {'color': 'k', 
                         'fontsize': 12,
                         'verticalalignment': 'center', 
                         'horizontalalignment': 'center'})

plt.text(R*1.9, p*1.3, "$V \\propto -\\frac{1}{r}$", {'color': 'k', 
                         'fontsize': 12,
                         'verticalalignment': 'center', 
                         'horizontalalignment': 'center'})

plt.text(-R*1.9, p*1.3, "$V \\propto -\\frac{1}{r}$", {'color': 'k', 
                         'fontsize': 12,
                         'verticalalignment': 'center', 
                         'horizontalalignment': 'center'})


#plt.title("Kvanttipisteen potentiaalin muoto")
plt.xlabel("$r$", size=12)
plt.ylabel("$V$", rotation=0, labelpad=10, size=12)  
                            # tikz_save does not work properly with rotation, 
                            # add in axis parameters a line: "y label style={rotate=-90},"
                            # add after \begin{tikzpicture}: "[scale=0.8]"
fig.tight_layout() # pad=1.5
plt.savefig("potentiaalin_muoto.pdf")
#tikz_save('potentiaalin_muoto.tikz')


plt.show()
