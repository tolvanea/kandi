## Kandidaatintyö
### Kvanttipistesoluautomaattien mallintaminen polkuintegraali-Monte Carlo -menetelmällä

TTY, Teknillinen fysiikka

Tämä repositorio sisältää kandin latex-tiedostot. Automatisoivat koodit löytyvät täältä:
[https://gitlab.com/tolvanea/QCA_calc]()

Tarkastajat:
* Tapio Rantala, 
* Juha Tiihonen


Alpi Tolvanen, 2018


Valmiksi käännetty pdf-versio on nimellä runko.pdf
    
